<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Test extends CI_Controller 
{

function __construct()
{
    parent::__construct();
}

function delete()
{
    $update_id = $this->uri->segment(3);
    $this->_delete($update_id);
    $flash_msg = "<p>The account was successfully deleted.</p>";
    $value= "<div class='alert alert-success' role='alert'>".$flash_msg."</div>" ;
    $this->session->set_flashdata('item', $value);
    redirect('test/manage');
}

function deleteconf()
{
    $update_id = $this->uri->segment(3);
    $data['update_id'] = $update_id;
    $data['headline'] = "Delete Account";
    $view_file = "deleteconf";
    $this->template($view_file, $data);
}

function create()
{
    $update_id = $this->uri->segment(3);
    $submit = $this->input->post('submit', TRUE);
    if ($submit=="Cancel") {
        redirect('test/manage');
    }
    if ($submit=="Submit") {
        //load the form_validation library
        $this->load->library('form_validation');
        //set validation rules
        //syntax: $this->form_validation->set_rules('fieldname','human name','validation rules')
        $this->form_validation->set_rules('firstname','First Name','required|min_length[3]|max_length[35]');
        $this->form_validation->set_rules('lastname','Last Name','required');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('company','Company','required');

        if ($this->form_validation->run() == TRUE) {
            //get variables
            $data = $this->fetch_data_from_post();
            if (is_numeric($update_id)) {
                //update
                $this->_update($update_id, $data);
                $flash_msg = "The account details were successfully updated.";
                $value= "<div class='alert alert-success' role='alert'>".$flash_msg."</div>" ;
                $this->session->set_flashdata('item', $value);
                redirect('test/create/'.$update_id);
            } else {
                //insert
                $this->_insert($data);
                $update_id = $this->get_max(); //get the ID of the new record
                $flash_msg = "The account was successfully added.";
                $value= "<div class='alert alert-success' role='alert'>".$flash_msg."</div>" ;
                $this->session->set_flashdata('item', $value);
                redirect('test/create/'.$update_id);
            }
        }
    }
    if (is_numeric($update_id) && ($submit != "Submit")) {
        $data = $this->fetch_data_from_db($update_id);
    }
    else {
        $data = $this->fetch_data_from_post();
    }

    if (!is_numeric($update_id)) {
        $data['headline'] = "Create New Account";
    }
    else {
        $data['headline'] = "Update Account Details";
    }
    $data['update_id'] = $update_id;
    $view_file = "create";
    $this->template($view_file, $data);
}

function fetch_data_from_post()
{
    $data['firstname'] = $this->input->post('firstname', TRUE);
    $data['lastname'] = $this->input->post('lastname', TRUE);
    $data['email'] = $this->input->post('email', TRUE);
    $data['company'] = $this->input->post('company', TRUE);
    return $data;
}

function fetch_data_from_db($update_id)
{
    $query = $this->get_where($update_id);
    foreach ($query->result() as $row) {
        $data['firstname'] = $row->firstname;
        $data['lastname'] = $row->lastname;
        $data['email'] = $row->email;
        $data['company'] = $row->company;
    }

    if(!isset($data)) {
        $data = "";
    }
    return $data;
}

function manage()
{
    $data['query'] = $this->get('lastname');
    $view_file = "manage";
    $this->template($view_file, $data);
}

function template($view_file, $data)
{
    $this->load->view('includes/admin_header');
    $this->load->view($view_file, $data);
    $this->load->view('includes/admin_footer');
}

function get($order_by)
{
    $this->load->model('mdl_test');
    $query = $this->mdl_test->get($order_by);
    return $query;
}

function get_with_limit($limit, $offset, $order_by)
{
    if ((!is_numeric($limit)) || (!is_numeric($offset))) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_test');
    $query = $this->mdl_test->get_with_limit($limit, $offset, $order_by);
    return $query;
}

function get_where($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_test');
    $query = $this->mdl_test->get_where($id);
    return $query;
}

function get_where_custom($col, $value)
{
    $this->load->model('mdl_test');
    $query = $this->mdl_test->get_where_custom($col, $value);
    return $query;
}

function _insert($data)
{
    $this->load->model('mdl_test');
    $this->mdl_test->_insert($data);
}

function _update($id, $data)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_test');
    $this->mdl_test->_update($id, $data);
}

function _delete($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_test');
    $this->mdl_test->_delete($id);
}

function count_where($column, $value)
{
    $this->load->model('mdl_test');
    $count = $this->mdl_test->count_where($column, $value);
    return $count;
}

function get_max()
{
    $this->load->model('mdl_test');
    $max_id = $this->mdl_test->get_max();
    return $max_id;
}

function _custom_query($mysql_query)
{
    $this->load->model('mdl_test');
    $query = $this->mdl_test->_custom_query($mysql_query);
    return $query;
}

}//end class