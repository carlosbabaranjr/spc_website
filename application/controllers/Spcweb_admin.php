<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Spcweb_admin extends CI_Controller 
{

function __construct()
{
    parent::__construct();
}

function index()
{
	$submit = $this->input->post('submit', TRUE);
    if ($submit == "Submit"){
        $this->form_validation->set_rules('username','Username','required|min_length[5]|max_length[60]|callback_username_check');
        $this->form_validation->set_rules('pword','Password','required|min_length[7]|max_length[35]');

        if ($this->form_validation->run() == TRUE) {
            
            $this->_in_you_go();
        }
    }

	$view_file = 'login/login';
    $this->login_template($view_file, $data);
}

function login_template($view_file, $data)
{
    $this->load->view('includes/login_header', $data);
    $this->load->view($view_file, $data);
    $this->load->view('includes/login_footer');
}

function _in_you_go()
{
    //set a session variable
    $this->session->set_userdata('is_admin', '1');
    
    //send the user to the private page
    redirect('dashboard/home');
}

function logout()
{
    unset($_SESSION['is_admin']);
    redirect(base_url());
}

function username_check($str)
{
    $error_msg = "You did not enter a correct username and/or password.";
    $pword = $this->input->post('pword', TRUE);

    $result = $this->_check_admin_login_details($str, $pword);

    if ($result==FALSE) {
        $this->form_validation->set_message('username_check', $error_msg);
        return FALSE;
    } else {
        return TRUE;
    }
}

function _check_admin_login_details($username, $pword)
{
	$target_username = "admin";
	$target_pass = "password";

	if (($username==$target_username) && ($pword==$target_pass)) {
		return TRUE;
	} else {
		return FALSE;
	}
}



}//end class