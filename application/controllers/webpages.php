<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webpages extends CI_Controller 
{

function __construct()
{
    parent::__construct();
}


function _make_sure_is_admin()
{
    $is_admin = $this->session->userdata('is_admin');
    if ($is_admin==1) {
        return TRUE;
    } else {
        redirect(base_url());
    }
}


function news()
{
    $page_url = $this->uri->segment(3);
    $data['query'] = $this->get_where_custom('page_url', $page_url);
    $view_file = 'webpages/content';
    $this->public_template($view_file, $data);
}

function aboutus()
{
    $data['additional_att2'] = 'class="active"';
    $data['additional_att1'] = ''; 
    $data['additional_att3'] = '';
    $data['query'] = $this->get_where_custom('page_url', 'about-us');
    $view_file = 'webpages/content';
    $this->public_template($view_file, $data);
}

function delete_page()
{
    $this->_make_sure_is_admin();
    $update_id = $this->uri->segment(3);
    $this->_delete($update_id);
    $flash_msg = "<p>The page was successfully deleted.</p>";
    $value= "<div class='alert alert-success' role='alert'>".$flash_msg."</div>" ;
    $this->session->set_flashdata('item', $value);
    redirect('webpages/manage');
}

function deleteconf_page()
{
    $this->_make_sure_is_admin();
    $update_id = $this->uri->segment(3);
    $data['update_id'] = $update_id;
    $data['delete_url'] = 'webpages/delete_page/'.$update_id;
    $data['headline'] = "Delete Page";
    $view_file = "webpages/deleteconf";
    $this->admin_template($view_file, $data);
}

function delete_img()
{
    $update_id = $this->uri->segment(3);
    $data = $this->fetch_data_from_db($update_id);
    $pic = $data['pic'];
    $pic_path = './img/carousel/'.$pic;

    //attempt to remove the image
    if (file_exists($pic_path)) {
        unlink($pic_path);
    }

    $data_pic['pic'] = '';
    $this->_update($update_id, $data_pic);
    $flash_msg = "<p>The image was successfully deleted.</p>";
    $value= "<div class='alert alert-success' role='alert'>".$flash_msg."</div>" ;
    $this->session->set_flashdata('item', $value);
    redirect('webpages/create/'.$update_id);
}

function deleteconf_img()
{
    $update_id = $this->uri->segment(3);
    $data['update_id'] = $update_id;
    $data['delete_url'] = 'webpages/delete_img/'.$update_id;
    $data['headline'] = "Delete Page Picture";
    $view_file = "webpages/deleteconf";
    $this->admin_template($view_file, $data);
}

function do_upload()
{
    $this->_make_sure_is_admin();
    $update_id = $this->uri->segment(3);
    $submit = $this->input->post('submit', TRUE);
    if ($submit == "Cancel") {
        redirect('webpages/create/'.$update_id);
    }
    $config['upload_path']          = './img/cms/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['max_size']             = 1000;
    $config['max_width']            = 640;
    $config['max_height']           = 470;

    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload('userfile')) {
        $data['error'] = array('error' => $this->upload->display_errors("<p class='alert alert-danger'>","</p>"));
        $data['headline'] = "Upload Error";
        $data['flash'] = $this->session->flashdata('item');
        $view_file = "webpages/upload_image";
        $this->admin_template($view_file, $data);
    } else {
        //upload was successful
        $data = array('upload_data' => $this->upload->data());

        $upload_data = $data['upload_data'];
        $file_name = $upload_data['file_name'];
        $data_carousel['pic'] = $file_name;
        $this->_update($update_id, $data_carousel);

        $data['update_id'] = $update_id;
        $data['headline'] = "Upload Success";
        $data['flash'] = $this->session->flashdata('item');
        $view_file = "webpages/upload_success";
        $this->admin_template($view_file, $data);
    }
}

function upload_image()
{
    $this->_make_sure_is_admin();
    $data['update_id'] = $this->uri->segment(3);
    $data['headline'] = "Upload Image";
    $data['flash'] = $this->session->flashdata('item');
    $view_file = "webpages/upload_image";
    $this->admin_template($view_file, $data);
}

function create()
{
    $this->_make_sure_is_admin();
    $update_id = $this->uri->segment(3);
    $submit = $this->input->post('submit', TRUE);
    if ($submit == "Cancel"){
        redirect('webpages/manage');
    }
    if ($submit == "Submit"){
        $this->form_validation->set_rules('page_title','Page Title','required|max_length[250]');
        $this->form_validation->set_rules('page_keywords','Page Keyword','required');
        $this->form_validation->set_rules('page_description','Page Description','required');
        $this->form_validation->set_rules('page_content','Page Content','required');

        if ($this->form_validation->run() == TRUE){
            //get variables
            $data = $this->fetch_data_from_post();
            $data['page_url'] = url_title($data['page_title']);

            if(is_numeric($update_id)){
                //update the page details
                    if($update_id < 3){
                        unset($data['page_url']);
                    }
                $this->_update($update_id, $data);
                $flash_msg = "The page details were successfully updated.";
                $value= "<div class='alert alert-success' role='alert'>".$flash_msg."</div>" ;
                $this->session->set_flashdata('item', $value);
                redirect('webpages/create/'.$update_id);
            }
            else{
                //insert a new page
                $this->_insert($data);
                $update_id = $this->get_max(); //get the ID of the new page
                $flash_msg = "The page was successfully added.";
                $value = "<div class='alert alert-success' role='alert'>".$flash_msg."</div>" ;
                $this->session->set_flashdata('item', $value);
                redirect('webpages/create/'.$update_id);
            }
        }
    }

    if( (is_numeric($update_id)) && ($submit !="Submit") ){
        $data = $this->fetch_data_from_db($update_id);
    }
    else{
        $data = $this->fetch_data_from_post();
    }

    if (!is_numeric($update_id)){
        $data['headline'] = "Create New Page";
    }
    else{
        $data['headline'] = "Update Page Details";
    }

    $data['update_id'] = $update_id;
    $data['flash'] = $this->session->flashdata('item');
    $view_file = 'webpages/create';
    $this->admin_template($view_file, $data);
}

function manage(){
    $this->_make_sure_is_admin();
    $data['flash'] = $this->session->flashdata('item');
    $this->load->model('mdl_webpages');
    $data['query'] = $this->mdl_webpages->get('page_url');
    $view_file = 'webpages/manage';
    $this->admin_template($view_file, $data);
}

function fetch_data_from_post()
{
    $data['page_title'] = $this->input->post('page_title', TRUE);
    $data['page_keywords'] = $this->input->post('page_keywords', TRUE);
    $data['page_description'] = $this->input->post('page_description', TRUE);
    $data['page_content'] = $this->input->post('page_content', TRUE);
    return $data;
}

function fetch_data_from_db($update_id)
{
    $query = $this->get_where($update_id);
    foreach ($query->result() as $row) {
        $data['page_title'] = $row->page_title;
        $data['page_url'] = $row->page_url;
        $data['page_keywords'] = $row->page_keywords;
        $data['page_description'] = $row->page_description;
        $data['page_content'] = $row->page_content;
        $data['pic'] = $row->pic;
    }

    if(!isset($data)){
        $data = "";
    }
    return $data;
}

function public_template($view_file, $data)
{
    $this->load->view('includes/public_header', $data);
    $this->load->view($view_file, $data);
    $this->load->view('includes/public_footer');
}

function admin_template($view_file, $data)
{
    $this->load->view('includes/admin_header');
    $this->load->view($view_file, $data);
    $this->load->view('includes/admin_footer');
}

function get($order_by)
{
    $this->load->model('mdl_webpages');
    $query = $this->mdl_webpages->get($order_by);
    return $query;
}

function get_with_limit($limit, $offset, $order_by)
{
    if ((!is_numeric($limit)) || (!is_numeric($offset))) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_webpages');
    $query = $this->mdl_webpages->get_with_limit($limit, $offset, $order_by);
    return $query;
}

function get_where($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_webpages');
    $query = $this->mdl_webpages->get_where($id);
    return $query;
}

function get_where_custom($col, $value)
{
    $this->load->model('mdl_webpages');
    $query = $this->mdl_webpages->get_where_custom($col, $value);
    return $query;
}

function _insert($data)
{
    $this->load->model('mdl_webpages');
    $this->mdl_webpages->_insert($data);
}

function _update($id, $data)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_webpages');
    $this->mdl_webpages->_update($id, $data);
}

function _delete($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_webpages');
    $this->mdl_webpages->_delete($id);
}

function count_where($column, $value)
{
    $this->load->model('mdl_webpages');
    $count = $this->mdl_webpages->count_where($column, $value);
    return $count;
}

function get_max()
{
    $this->load->model('mdl_webpages');
    $max_id = $this->mdl_webpages->get_max();
    return $max_id;
}

function _custom_query($mysql_query)
{
    $this->load->model('mdl_webpages');
    $query = $this->mdl_webpages->_custom_query($mysql_query);
    return $query;
}

}//end class