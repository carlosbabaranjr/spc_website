<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Register extends CI_Controller 
{

function __construct()
{
    parent::__construct();
}

function signup()
{
    $this->load->view('register/register.php');
}
function head()
{
    $this->load->view('includes/pub_header');
    $this->load->view('create_research');
    $this->load->view('includes/pub_footer');
}

/*function SignUp()
{
    $update_id = $this->uri->segment(3);
    $submit = $this->input->post('submit', TRUE);
    if ($submit=="Cancel") {
        redirect('login/');
    }
    if ($submit=="Submit") {
        //get variables
        $data = $this->fetch_data_from_post_personal();
        //insert
        $this->_insert($data);
        $update_id = $this->get_max(); //get the ID of the new record
        redirect('login/');
    }
    $data = $this->fetch_data_from_post_personal();
    $data['update_id'] = $update_id;
    $view_file = "personal_info";
    $this->template($view_file, $data);
}*/

function test(){
     $hash = $this->input->post('question', TRUE). $this->input->post('email', TRUE);
    $pw = md5($this->input->post('password', TRUE));
    $salt = $pw . $hash;
    $enc_salt = hash('sha256', $salt);  
    $enc_password = substr($enc_salt, 0, 250);

    echo $this->input->post('email', TRUE) . "</br>".
         $this->input->post('question', TRUE) . "</br>".
         $this->input->post('account_type', TRUE) . "</br>".
         $enc_password;
}

function index()
{
    $update_id = $this->uri->segment(3);
    $submit = $this->input->post('submit', TRUE);
    if ($submit=="Cancel") {
        redirect('login/');
    }
    if ($submit=="Submit") {
        //get variables
        $data = $this->fetch_data_from_post();
        //insert
        $this->_insert($data);
        $update_id = $this->get_max(); //get the ID of the new record
        redirect('Register/SignUp/'.$update_id);
    }
    if (is_numeric($update_id) && ($submit != "Submit")) {
    
    }
    else {
        $data = $this->fetch_data_from_post();
    }
    $data['update_id'] = $update_id;
    $view_file = "register";
    $this->template($view_file, $data);
}

function add()
{
    $data = $this->fetch_data_from_post();
    $this->_insert($data);
    echo 'Done!';
}

function fetch_data_from_post()
{
    $hash = $this->input->post('question', TRUE). $this->input->post('email', TRUE);
    $pw = md5($this->input->post('password', TRUE));
    $salt = $pw . $hash;
    $enc_salt = hash('sha256', $salt);  
    $enc_password = substr($enc_salt, 0, 250);

    $data['email'] = $this->input->post('email', TRUE);
    $data['password'] = $enc_password;
    $data['hash'] = $this->input->post('question', TRUE);
    $data['account_type'] = $this->input->post('account_type', TRUE);
    $data['attempts'] = "0";
    return $data;
}

function fetch_data_from_post_personal()
{
    $update_id = $this->uri->segment(3);

    $data['id'] = $update_id;
    $data['unit_id'] = $this->input->post('unit_id', TRUE);
    $data['course_id'] = $this->input->post('course_id', TRUE);
    $data['student_num'] = $this->input->post('student_num', TRUE);
    $data['firstname'] = $this->input->post('firstname', TRUE);
    $data['middlename'] = $this->input->post('middlename', TRUE);
    $data['lastname'] = $this->input->post('lastname', TRUE);
    $data['birthday'] = $this->input->post('birthday', TRUE);
    $data['contactnum'] = $this->input->post('contactnum', TRUE);
    $data['prefix'] = $this->input->post('prefix', TRUE);
    return $data;
}

function template($view_file, $data)
{
    $this->load->view('includes/login_header');
    $this->load->view($view_file, $data);
    $this->load->view('includes/login_footer');
}

function get($order_by)
{
    $this->load->model('mdl_register');
    $query = $this->mdl_register->get($order_by);
    return $query;
}

function get_with_limit($limit, $offset, $order_by)
{
    if ((!is_numeric($limit)) || (!is_numeric($offset))) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_register');
    $query = $this->mdl_register->get_with_limit($limit, $offset, $order_by);
    return $query;
}

function get_where($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_register');
    $query = $this->mdl_register->get_where($id);
    return $query;
}

function get_where_custom($col, $value)
{
    $this->load->model('mdl_register');
    $query = $this->mdl_register->get_where_custom($col, $value);
    return $query;
}

function _insert($data)
{
    $this->load->model('mdl_register');
    $this->mdl_register->_insert($data);
}

function _update($id, $data)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_register');
    $this->mdl_register->_update($id, $data);
}

function _delete($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_register');
    $this->mdl_register->_delete($id);
}

function count_where($column, $value)
{
    $this->load->model('mdl_register');
    $count = $this->mdl_register->count_where($column, $value);
    return $count;
}

function get_max()
{
    $this->load->model('mdl_register');
    $max_id = $this->mdl_register->get_max();
    return $max_id;
}

function _custom_query($mysql_query)
{
    $this->load->model('mdl_register');
    $query = $this->mdl_register->_custom_query($mysql_query);
    return $query;
}

}//end class