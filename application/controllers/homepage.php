<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends CI_Controller 
{

function __construct()
{
    parent::__construct();
}

function index()
{
    $data['query_carousel'] = $this->get_carousel('id');
    $data['num_rows_carousel'] = $data['query_carousel']->num_rows();

    $data['query_webpages'] = $this->get_webpages('id');
    $data['num_rows_webpages'] = $data['query_webpages']->num_rows();
    $data['additional_att1'] = 'class="active"';
    $data['additional_att2'] = ''; 
    $data['additional_att3'] = '';
    $view_file = 'homepage';
    $this->public_template($view_file, $data);
}

function get_carousel($order_by)
{
    $this->load->model('mdl_homepage');
    $query = $this->mdl_homepage->get_carousel($order_by);
    return $query;
}

function get_webpages($order_by)
{
    $this->load->model('mdl_homepage');
    $query = $this->mdl_homepage->get_webpages($order_by);
    return $query;
}

function public_template($view_file, $data)
{
    $this->load->view('includes/public_header', $data);
    $this->load->view($view_file, $data);
    $this->load->view('includes/public_footer');
}

function admin_template($view_file, $data)
{
    $this->load->view('includes/admin_header');
    $this->load->view($view_file, $data);
    $this->load->view('includes/admin_footer');
}


function get_with_limit($limit, $offset, $order_by)
{
    if ((!is_numeric($limit)) || (!is_numeric($offset))) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_homepage');
    $query = $this->mdl_homepage->get_with_limit($limit, $offset, $order_by);
    return $query;
}

function get_where($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_homepage');
    $query = $this->mdl_homepage->get_where($id);
    return $query;
}

function get_where_custom($col, $value)
{
    $this->load->model('mdl_homepage');
    $query = $this->mdl_homepage->get_where_custom($col, $value);
    return $query;
}

function _insert($data)
{
    $this->load->model('mdl_homepage');
    $this->mdl_homepage->_insert($data);
}

function _update($id, $data)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_homepage');
    $this->mdl_homepage->_update($id, $data);
}

function _delete($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_homepage');
    $this->mdl_homepage->_delete($id);
}

function count_where($column, $value)
{
    $this->load->model('mdl_homepage');
    $count = $this->mdl_homepage->count_where($column, $value);
    return $count;
}

function get_max()
{
    $this->load->model('mdl_homepage');
    $max_id = $this->mdl_homepage->get_max();
    return $max_id;
}

function _custom_query($mysql_query)
{
    $this->load->model('mdl_homepage');
    $query = $this->mdl_homepage->_custom_query($mysql_query);
    return $query;
}

}//end class