<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{

function __construct()
{
    parent::__construct();
}

function _make_sure_is_admin()
{
    $is_admin = $this->session->userdata('is_admin');
    if ($is_admin==1) {
        return TRUE;
    } else {
        redirect(base_url());
    }
}

function home()
{
	$this->_make_sure_is_admin();
	$view_file = 'dashboard/home';
    $this->admin_template($view_file, $data);
}

function admin_template($view_file, $data)
{
    $this->load->view('includes/admin_header');
    $this->load->view($view_file, $data);
    $this->load->view('includes/admin_footer');
}

}