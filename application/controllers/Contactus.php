<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactus extends CI_Controller 
{

function __construct()
{
    parent::__construct();
}

function _get_map_code()
{
    $code = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1009385.4379614287!2d125.57694776989223!3d-8.794350523870625!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2cfde50986e4a129%3A0x3e5c68387e85b3c!2sTimor-Leste!5e0!3m2!1sen!2sph!4v1483625622084" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';
    return $code;
}

function _get_our_name()
{
    $name = 'Sisters of St. Paul of Chartres<br>East Timor Region';
    return $name;
}

function _get_our_address()
{
    $address = 'Timor Leste,<br>Indonesia';
    return $address;
}

function _get_our_telnum()
{
    $telnum = '39 0666418936';
    return $telnum;
}

function index()
{
    $data['our_address'] = $this->_get_our_address();
    $data['our_telnum'] = $this->_get_our_telnum();
    $data['our_name'] = $this->_get_our_name();
    $data['map_code'] = $this->_get_map_code();
    $data['additional_att3'] = 'class="active"';
    $data['additional_att2'] = ''; 
    $data['additional_att1'] = '';
    $view_file = 'contactus/contactus';
    $this->public_template($view_file, $data);
}

function public_template($view_file, $data)
{
    $this->load->view('includes/public_header', $data);
    $this->load->view($view_file, $data);
    $this->load->view('includes/public_footer');
}

}//end class