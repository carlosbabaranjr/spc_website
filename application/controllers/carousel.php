<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Carousel extends CI_Controller 
{

function __construct()
{
    parent::__construct();
}

function delete()
{
    $update_id = $this->uri->segment(3);
    $data = $this->fetch_data_from_db($update_id);
    $pic = $data['pic'];
    $pic_path = './img/carousel/'.$pic;

    //attempt to remove the image
    if (file_exists($pic_path)) {
        unlink($pic_path);
    }

    $this->_delete($update_id);
    $flash_msg = "<p>The image was successfully deleted.</p>";
    $value= "<div class='alert alert-success' role='alert'>".$flash_msg."</div>" ;
    $this->session->set_flashdata('item', $value);
    redirect('carousel/manage');
}

function deleteconf()
{
    $update_id = $this->uri->segment(3);
    $data['update_id'] = $update_id;
    $data['headline'] = "Delete Image";
    $view_file = "carousel/deleteconf";
    $this->admin_template($view_file, $data);
}

function _make_sure_is_admin()
{
    $is_admin = $this->session->userdata('is_admin');
    if ($is_admin==1) {
        return TRUE;
    } else {
        redirect(base_url());
    }
}


function manage()
{
    $this->_make_sure_is_admin();
    $data['query'] = $this->get('id');
    $view_file = "carousel/manage";
    $this->admin_template($view_file, $data);
}

function do_upload()
{
    $submit = $this->input->post('submit', TRUE);
    if ($submit == "Cancel") {
        redirect('carousel/manage');
    }
    $config['upload_path']          = './img/carousel/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['max_size']             = 1000;
    $config['max_width']            = 1280;
    $config['max_height']           = 560;

    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload('userfile')) {
        $data['error'] = array('error' => $this->upload->display_errors("<p class='alert alert-danger'>","</p>"));
        $data['headline'] = "Upload Error";
        $data['flash'] = $this->session->flashdata('item');
        $view_file = "carousel/upload_image";
        $this->admin_template($view_file, $data);
    } else {
        //upload was successful
        $data = array('upload_data' => $this->upload->data());

        $upload_data = $data['upload_data'];
        $file_name = $upload_data['file_name'];
        $data_carousel['pic'] = $file_name;
        $this->_insert($data_carousel);

        $data['headline'] = "Upload Success";
        $data['flash'] = $this->session->flashdata('item');
        $view_file = "carousel/upload_success";
        $this->admin_template($view_file, $data);
    }
}

function upload_image()
{
    $this->_make_sure_is_admin();
    $data['headline'] = "Upload Image";
    $data['flash'] = $this->session->flashdata('item');
    $view_file = "carousel/upload_image";
    $this->admin_template($view_file, $data);
}

function home()
{
    $this->_make_sure_is_admin();
    $data['query'] = $this->get('id');
    $data['num_rows'] = $data['query']->num_rows();
    $view_file = 'carousel/carousel';
    $this->public_template($view_file, $data);
}

function public_template($view_file, $data)
{
    $this->load->view('includes/public_header');
    $this->load->view($view_file, $data);
    $this->load->view('includes/public_footer');
}

function admin_template($view_file, $data)
{
    $this->load->view('includes/admin_header');
    $this->load->view($view_file, $data);
    $this->load->view('includes/admin_footer');
}

function fetch_data_from_db($update_id)
{
    $query = $this->get_where($update_id);
    foreach ($query->result() as $row) {
        $data['pic'] = $row->pic;
    }

    if(!isset($data)) {
        $data = "";
    }
    return $data;
}

function get($order_by)
{
    $this->load->model('mdl_carousel');
    $query = $this->mdl_carousel->get($order_by);
    return $query;
}

function get_with_limit($limit, $offset, $order_by)
{
    if ((!is_numeric($limit)) || (!is_numeric($offset))) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_carousel');
    $query = $this->mdl_carousel->get_with_limit($limit, $offset, $order_by);
    return $query;
}

function get_where($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_carousel');
    $query = $this->mdl_carousel->get_where($id);
    return $query;
}

function get_where_custom($col, $value)
{
    $this->load->model('mdl_carousel');
    $query = $this->mdl_carousel->get_where_custom($col, $value);
    return $query;
}

function _insert($data)
{
    $this->load->model('mdl_carousel');
    $this->mdl_carousel->_insert($data);
}

function _update($id, $data)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_carousel');
    $this->mdl_carousel->_update($id, $data);
}

function _delete($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_carousel');
    $this->mdl_carousel->_delete($id);
}

function count_where($column, $value)
{
    $this->load->model('mdl_carousel');
    $count = $this->mdl_carousel->count_where($column, $value);
    return $count;
}

function get_max()
{
    $this->load->model('mdl_carousel');
    $max_id = $this->mdl_carousel->get_max();
    return $max_id;
}

function _custom_query($mysql_query)
{
    $this->load->model('mdl_carousel');
    $query = $this->mdl_carousel->_custom_query($mysql_query);
    return $query;
}

}//end class