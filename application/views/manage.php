<div class='col-md-12'>
    <div class='box box-info'>
        <div class='box-body pad'>
        <div>
            <a href="<?= base_url().'test/create';?>">
                <button class="btn btn-success">ADD ACCOUNT</button>
            </a>
        </div>
        <br>
            <?php
                $flash = $this->session->flashdata('item');
                if (isset($flash)) {
                    echo $flash;
                }
            ?>
        	<table class="table table-striped table-bordered">
        		<thead>
        			<tr>
        				<th>Firstname</th>
        				<th>Lastname</th>
        				<th>Email</th>
        				<th>Company</th>
                        <th>Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			<?php
		        		foreach ($query->result() as $row) {
		        			$firstname = $row->firstname;
		        			$lastname = $row->lastname;
		        			$email = $row->email;
		        			$company = $row->company;
                            $edit_acct_url = base_url().'test/create/'.$row->id;
		        		?>
		        			<tr>
		        				<td> <?= $firstname ?> </td>
		        				<td> <?= $lastname ?> </td>
		        				<td> <?= $email ?></td>
		        				<td> <?= $company ?></td>
                                <td>
                                    <a href="<?= $edit_acct_url ?>">
                                        <i class="fa fa-edit"></i> EDIT
                                    </a>
                                </td>
		        			</tr>
		        		<?php } ?>
        		</tbody>
        	</table>
        	
        </div>
    </div>
</div>