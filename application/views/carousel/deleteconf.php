<div class='box box-info'>
    <div class='box-body pad'>
		<h1 class="page-header"><?= $headline ?></h1>
		<p>
			Are you sure you want to delete the image?
		</p>
		
		<a href="<?= base_url(). 'carousel/delete/'.$update_id; ?>">
			<button class="btn btn-success">Yes - Delete Image</button>
		</a>
		&nbsp; &nbsp;
		<a href="<?= base_url(). 'carousel/manage' ?>">
			<button class="btn btn-default">Cancel</button>
		</a>
	</div>
</div>