<div class='col-md-12'>
    <div class='box box-info'>
        <div class='box-body pad'>
        <div>
            <a href="<?= base_url().'carousel/upload_image';?>">
                <button class="btn btn-success">UPLOAD IMAGE</button>
            </a>
        </div>
        <br>
            <h3><a href="<?= base_url() ?>carousel/home">VIEW PAGE</a></h3>
            <?php
                $flash = $this->session->flashdata('item');
                if (isset($flash)) {
                    echo $flash;
                }
            ?>
        	<table class="table table-striped table-bordered">
        		<thead>
        			<tr>
        				<th></th>
        				<th>Filename</th>
                        <th>Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			<?php
		        		foreach ($query->result() as $row) {
		        			$filename = $row->pic;
                            $delete_url = base_url().'carousel/deleteconf/'.$row->id;
		        		?>
		        			<tr>
		        				<td class="col-md-3">
                                    <img src="<?= base_url().'img/carousel/'.$filename ?>" width="200" height="88">                  
                                </td>
		        				<td> <?= $filename ?> </td>
                                <td>
                                    <a href="<?= $delete_url ?>" class="btn btn-danger">
                                        DELETE
                                    </a>
                                </td>
		        			</tr>
		        		<?php } ?>
        		</tbody>
        	</table>
        	
        </div>
    </div>
</div>