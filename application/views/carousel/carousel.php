<?php if ($num_rows>=1) { ?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
      $i = 0;
      foreach ($query->result() as $row) {
    ?>

    <?php if ($i==0) {
      echo '<li data-target="#myCarousel" data-slide-to="'.$i.'" class="active"></li>';
    } else {
      echo '<li data-target="#myCarousel" data-slide-to="'.$i.'"></li>';
    }
    $i++;
    } 
    ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php
      $i = 0;
      foreach ($query->result() as $row) {
    ?>

    <?php if ($i==0) { ?>
      <div class="item active">
        <img src="<?= base_url().'img/carousel/'.$row->pic ?>" alt="<?= $row->pic ?>" width="460" height="345">
      </div>
    <?php } else { ?>
      <div class="item">
        <img src="<?= base_url().'img/carousel/'.$row->pic ?>" alt="<?= $row->pic ?>" width="460" height="345">
      </div>
    <?php } 
    $i++;
    } ?>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<?php } else { ?>
<h1>No Images Available</h1>
<a href="<?= base_url() ?>carousel/upload_image"><h3>Upload an image by Clicking Here</h3></a>
<?php } ?>