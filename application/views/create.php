<div class='col-md-12'>
	<?php if(isset($update_id)) { ?>
	<div class='box box-info'>
        <div class='box-body pad'>
        	<h1 class="page-header"> Account Option</h1>
        	<?php $delete_acct_url = base_url().'test/deleteconf/'.$update_id;
        	?>
        	<a href="<?= $delete_acct_url ?>">
        		<button class="btn btn-danger">Delete Account</button>
        	</a>
        </div>
     </div>
     <?php } ?>

    <div class='box box-info'>
        <div class='box-body pad'>
        	<h1 class="page-header"> <?= $headline ?> </h1>
        	<?php
        		$flash = $this->session->flashdata('item');
        		if (isset($flash)) {
        			echo $flash;
        		}
        	?>
        	<?= validation_errors('<p class="alert alert-danger">',"</p>") ?>
            <?php
            	$form_location = base_url()."test/create/".$update_id;
            ?>
			<form role="form" method="post" action="<?= $form_location ?>">
			    <div class="box-body">
			    	<div class="form-group">
			            <label>First Name</label>
			            <input type="text" name="firstname" value="<?= $firstname ?>" class="form-control" placeholder="Enter first name">
			        </div>

			        <div class="form-group">
			            <label>Last Name</label>
			            <input type="text" name="lastname" value="<?= $lastname ?>" class="form-control" placeholder="Enter last name">
			        </div>

			        <div class="form-group">
			            <label>Email address</label>
			            <input type="text" name="email" value="<?= $email ?>" class="form-control" placeholder="Enter email">
			        </div>

			        <div class="form-group">
			            <label>Company</label>
			            <input type="text" name="company" value="<?= $company ?>" class="form-control" placeholder="Enter company">
			        </div>
			    </div><!-- /.box-body -->

			    <div class="box-footer">
			        <button type="submit" name="submit" value="Submit" class="btn btn-primary">Submit</button>
			        &nbsp;
			        <button type="submit" name="submit" value="Cancel" class="btn btn-primary">Cancel</button>
			    </div>
			</form>

        </div>
    </div><!-- /.box -->
</div><!-- /.col-->