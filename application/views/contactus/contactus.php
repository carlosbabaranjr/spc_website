<style type="text/css">
.map-responsive{
  overflow:hidden;
  padding-bottom:56.25%;
  position:relative;
  height:0;
}
.map-responsive iframe{
  left:0;
  top:0;
  height:100%;
  width:100%;
  position:absolute;
}
</style>
<div class="row">
  <div class="col-md-12" style="margin-top: -25px;">
    <h1 class="page-header">Contact Us</h1>
    <div style="clear: both;">
        
      <div class="container">
          <div class="row">
              <div class="col-md-8">
                  <div class="well well-sm">

                      <form action="#" method="post">
                      <div class="row">
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label for="name">
                                      Name</label>
                                  <input type="text" name="yourname" class="form-control" id="name" placeholder="Enter name" required="required" autocomplete="off"/>
                              </div>
                              <div class="form-group">
                                  <label for="email">
                                      Email Address</label>
                                  <div class="input-group">
                                      <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                      </span>
                                      <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" required="required" autocomplete="off"/></div>
                              </div>
                              <div class="form-group">
                                  <label for="subject">
                                      Telephone Number</label><div class="input-group">
                                      <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span>
                                      </span>
                                      <input type="text" name="telnum" class="form-control" id="email" placeholder="Enter telephone number" required="required" autocomplete="off"/></div>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <label for="name">
                                      Message</label>
                                  <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                      placeholder="Message"></textarea>
                              </div>
                          </div>
                          <div class="col-md-12">
                              <button type="submit" name="submit" value="Submit" class="btn btn-primary pull-right" id="btnContactUs">
                                  Send Message</button>
                          </div>
                      </div>
                      </form>
                  </div>
              </div>
              <div class="col-md-4">
                  <form>
                  <legend><span class="glyphicon glyphicon-globe"></span> Our office</legend>
                  <address>
                      <strong><?= $our_name ?></strong><br>
                      <?= $our_address ?>
                      <abbr title="Phone">
                          P:</abbr>
                      <?= $our_telnum ?>
                  </address>
                  <address>
                      <strong>Telephone</strong><br>
                      <?= $our_telnum ?>
                  </address>
                  </form>
              </div>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="map-responsive">
            <?= $map_code ?>
          </div>
        </div>
      </div>


    </div>
  </div>
</div>