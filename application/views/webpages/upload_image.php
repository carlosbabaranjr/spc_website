<h1 class="page-header"><?= $headline ?></h1>

<div class='box box-info'>
    <div class='box-body pad'>
	<?php 
		if(isset($error)){
			foreach ($error as $value) {
				echo $value;
			}
		}
	?>
	<?php 
		$attributes = array('class' => 'form-horizontal');
		echo form_open_multipart('webpages/do_upload/'.$update_id, $attributes); 
	?>
	<p style="margin-top:24px;">
		Please choose a file from your computer and then press 'Upload'.
	</p>
	  <fieldset>
		<div class="control-group" style="height:200px;">
		  <label class="control-label" for="fileInput">File input</label>
		  <div class="controls">
			<input class="input-file uniform_on" id="fileInput" name="userfile" type="file">
		  </div>
		</div>       
		<div class="form-actions">
		  <button type="submit" class="btn btn-primary">Upload</button>
		  <button type="submit" name="submit" value="Cancel" class="btn">Cancel</button>
		</div>
	  </fieldset>
	</form>   
	</div>
</div>