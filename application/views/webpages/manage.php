<h1 class="page-header">Content Management System</h1>

<div class='box box-info'>
    <div class='box-body pad'>

		<?php
		if(isset($flash)){
			echo $flash;
		}	?>

		<?php $create_page_url = base_url()."webpages/create"; ?>
		<p>
			<a href="<?= $create_page_url ?>">
				<button type="button" class="btn btn-primary">Create New Webpage</button>
			</a>
		</p>
		<table class="table table-striped table-bordered bootstrap-datatable datatable">
			  <thead>
				  <tr>
					  <th>Page URL</th>
					  <th>Page Title</th>
					  <th class="span2">Actions</th>
				  </tr>
			  </thead>   
			  <tbody>
			  <?php foreach($query->result() as $row) {
			  		$edit_page_url = base_url()."webpages/create/".$row->id;
			  		$view_page_url = base_url().'webpages/news/'.$row->page_url;
			  ?>
				<tr>
					<td class="col-md-4"><?= $view_page_url ?></td>
					<td class="col-md-5"><?= $row->page_title ?></td>
					<td class="center">
						<a class="btn btn-success btn-sm" href="<?= $view_page_url ?>">
							VIEW PAGE
						</a>
						<a class="btn btn-info btn-sm" href="<?= $edit_page_url ?>">
							EDIT PAGE
						</a>
					</td>
				</tr>
				<?php } ?>
			  </tbody>
		  </table>            
    </div>
</div>