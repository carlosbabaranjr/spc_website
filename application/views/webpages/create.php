<h1 class="page-header"><?= $headline ?></h1>

<?php if (is_numeric($update_id)) { ?>
<div class='box box-info'>
    <div class='box-body pad' style="min-height: 60px;">
    	<?php
    		$upload_url = base_url().'webpages/upload_image/'.$update_id;
    		if ($pic=="") { ?>
    			<a class="btn btn-success btn-sm" href="<?= $upload_url ?>">
					UPLOAD PAGE PIC
				</a>
    		<?php }  else { ?>
    			<a class="btn btn-success btn-sm" href="<?= $upload_url ?>">
					UPDATE PAGE PIC
				</a>
				<a class="btn btn-danger btn-sm" href="<?= base_url().'webpages/deleteconf_img/'.$update_id ?>">
					DELETE IMAGE
				</a>
    		<?php } ?>
    			<a class="btn btn-danger btn-sm" href="<?= base_url().'webpages/deleteconf_page/'.$update_id ?>">
					DELETE PAGE
				</a>
    </div>
</div>
<?php } ?>

<div class='box box-info'>
    <div class='box-body pad'>
    	<?= validation_errors('<p class="alert alert-danger">') ?>

		<?php
		if(isset($flash)){
			echo $flash;
		}	?>

		<?php $form_location = base_url()."webpages/create/".$update_id;?>  
		<form role="form" method="post" action="<?= $form_location ?>">
		    <div class="box-body">
		    	<div class="form-group">
		            <label>Page Title</label>
		            <input type="text" name="page_title" value="<?= $page_title ?>" class="form-control">
		        </div>

		        <div class="form-group">
		            <label>Page Keywords</label>
		            <input type="text" name="page_keywords" value="<?= $page_keywords ?>" class="form-control">
		        </div>

		        <div class="form-group">
		            <label>Page Description</label>
		            <textarea row="3" class="form-control" name="page_description" style="resize:none;height:100px;"><?php echo $page_description; ?></textarea>
		        </div>

		        <div class="form-group">
		            <label>Page Content</label>
		            <textarea name="page_content" id="editor1"><?php echo $page_content; ?></textarea>
		        </div>
		    </div><!-- /.box-body -->

		    <div class="box-footer">
		        <button type="submit" name="submit" value="Submit" class="btn btn-primary">Submit</button>
		        &nbsp;
		        <button type="submit" name="submit" value="Cancel" class="btn btn-primary">Cancel</button>
		    </div>
		</form>		
    </div>
</div>

<?php if ($pic!="") { ?>
<div class='box box-info'>
    <div class='box-body pad'>
    <img src="<?= base_url().'img/cms/'.$pic ?>">
    </div>
</div>
<?php } ?>