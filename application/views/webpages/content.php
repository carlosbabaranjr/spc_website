<div class="panel panel-default">
  <div class="panel-body">
  	<?php
  	foreach($query->result() as $row) {
  		$page_title = $row->page_title;
  		$pic = $row->pic;
  		$page_content = $row->page_content ;
  	}
  	?>
  	<h1 class="page-header" style="margin-top: 0px;"><?= $page_title ?></h1>
  	<p style="text-align: center;">
  		<img src="<?= base_url().'img/cms/'.$pic ?>" class="img-responsive" style="width: 50%;margin:0 auto;">
  	</p>
  	<p>
  		<?= $page_content ?>
  	</p>
</div>
</div>