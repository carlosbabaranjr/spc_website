<h1 class="page-header"><?= $headline ?></h1>

<div class="panel panel-default">
  	<div class="panel-body">
  		<div class="alert alert-success">Your file was successfully uploaded!</div>
			<ul>
			<?php foreach ($upload_data as $item => $value) { ?>
				<li><?php echo $item;?>: <?php echo $value;?></li>
			<?php } ?>
			</ul>

			<p>
				<?php $edit_item_url = base_url()."webpages/create/".$update_id;?>
				<a href="<?= $edit_item_url ?>"><button type="button" class="btn btn-primary">Return to Update Page Details</button></a>
			</p>
	</div>
</div>