<div class='box box-info'>
    <div class='box-body pad'>
		<h1 class="page-header"><?= $headline ?></h1>
		<p>
			Are you sure you want to delete?
		</p>
		
		<a href="<?= base_url().$delete_url; ?>">
			<button class="btn btn-success">Yes - Delete</button>
		</a>
		&nbsp; &nbsp;
		<a href="<?= base_url(). 'webpages/create/'.$update_id; ?>">
			<button class="btn btn-default">Cancel</button>
		</a>
	</div>
</div>