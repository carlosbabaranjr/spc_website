<!DOCTYPE html>
<html lang="en">
<head>
  <title>SPC East Timor</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap.min.css">
  <script src="<?= base_url() ?>public/jquery.min.js"></script>
  <script src="<?= base_url() ?>public/bootstrap.min.js"></script>
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 100%;
      margin: auto;
  }
  </style>
</head>

  <body>

    <div class="container">
    <br>
      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid" style="padding: 0px;">
          <img src="<?= base_url() ?>img/header2.jpg" style="width: 100%;">
        </div>
        <div class="container-fluid" style="border-top: solid 2px #fff;">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url() ?>homepage">SPC</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li <?= $additional_att1 ?>><a href="<?= base_url() ?>homepage">Home</a></li>
              <li <?= $additional_att2 ?>><a href="<?= base_url() ?>webpages/aboutus">About Us</a></li>
              <li <?= $additional_att3 ?>><a href="<?= base_url() ?>contactus">Contact Us</a></li>
            </ul>

            <!-- logout 
            <ul class="nav navbar-nav navbar-right">
              <li><a href="../navbar-fixed-top/">Log Out</a></li>
            </ul>
            --> 
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
