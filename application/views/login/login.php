<div class="form-box" id="login-box">
	<p style="text-align: center;">
		<img src="<?= base_url() ?>img/coat_of_arms.gif" style="width: 150px;">
	</p>
	<?= validation_errors('<p class="alert alert-danger" style="margin-left: 0px;">','</p>') ?>
    <div class="header">Sign In</div>
    <form action="<?= base_url() ?>spcweb_admin" method="post">
        <div class="body bg-gray">
            <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="User ID"/>
            </div>
            <div class="form-group">
                <input type="password" name="pword" class="form-control" placeholder="Password"/>
            </div>
        </div>
        <div class="footer">                                                               
            <button type="submit" name="submit" value="Submit" class="btn bg-olive btn-block">Sign me in</button> 
        </div>
    </form>
</div>