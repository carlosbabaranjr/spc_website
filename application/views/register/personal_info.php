<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>WEAVER | Registration</h3>
                <small>Research Management System</small>
            </div>
            <div class="hpanel" style="background: #fff;">
                <div class="panel-body">
                        <form method="post" role="form" action="<?= base_url() . 'Register/'; ?>" id="loginForm">
                             <div class="form-group">
                                <label class="control-label" for="password">Student Number</label>
                                <input type="text" title="Please enter your password" placeholder="Student Number" required="" value="" name="password" id="password" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label" for="username">First Name</label>
                                <input type="text" placeholder="First Name" title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
                                <!-- <span class="help-block small">Your unique username to app</span> -->
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label" for="username">Middle Name</label>
                                <input type="text" placeholder="Middle Name" title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
                                <!-- <span class="help-block small">Your unique username to app</span> -->
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label" for="username">Last Name</label>
                                <input type="text" placeholder="Last Name" title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
                                <!-- <span class="help-block small">Your unique username to app</span> -->
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label" for="username">Birth Date</label>
                                <input type="text" placeholder="MM/DD/YYYY" title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
                                <!-- <span class="help-block small">Your unique username to app</span> -->
                            </div>
                            <div class="form-group">
                                <label>Department</label>
                                  <select name="account_type" tabindex="5" class="form-control">
                                    <option selected="true" disabled="disabled"> Select Option </option>
                                    <option value="2">Researcher / Student</option>
                                    <option value="3">Adviser</option>
                                  </select>
                            </div>
                            <div class="form-group">
                                <label>Course</label>
                                  <select name="account_type" tabindex="5" class="form-control">
                                    <option selected="true" disabled="disabled"> Select Option </option>
                                    <option value="2">Researcher / Student</option>
                                    <option value="3">Adviser</option>
                                  </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">Contact Number</label>
                                <input type="password" title="Please enter your password" placeholder="Contact Number" required="" value="" name="password" id="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">Prefix</label>
                                <input type="password" title="Please enter your password" placeholder="eg. Mr. , Mrs. , Dr. , Atty. etc." required="" value="" name="hash" id="password" class="form-control">
                            </div>
                            <!-- <div class="checkbox">
                                <input type="checkbox" class="i-checks" checked>
                                     Remember login
                                <p class="help-block small">(if this is a private computer)</p>
                            </div> -->
                            <button class="btn btn-success btn-block">Confirm</button>
                            <a class="btn btn-default btn-block" href="<?= base_url() . 'login/'; ?>">Login</a>
                        </form>
                </div>
            </div>
        </div>
    </div>
    