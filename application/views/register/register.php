<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>WEAVER | Registration</h3>
                <small>Research Management System</small>
            </div>
            <div class="hpanel" style="background: #fff;">
                <div class="panel-body">
                    <?php
                        $form_location = base_url()."Register/add";
                    ?>
                        <form action="<?= $form_location ?>" role="form" method="post" id="loginForm">
                            <div class="form-group">
                                <label class="control-label" for="username">Email</label>
                                <input type="text" placeholder="example@gmail.com" title="Please enter you username" name="email" id="email" class="form-control">
                                <!-- <span class="help-block small">Your unique username to app</span> -->
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">Password</label>
                                <input type="password" title="Please enter your password" placeholder="******" name="password" id="password" class="form-control">
                                <span class="help-block small"><p id="validateerror" style="font-style: italic;"></p></span>
                                <span class="help-block small"><p id="validateerror1" style="font-style: italic;"></p></span>
                                <span class="help-block small"><p id="validateerror2" style="font-style: italic;"></p></span>
                                <span class="help-block small"><p id="validateerror3" style="font-style: italic;"></p></span>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">Confirm Password</label>
                                <input type="password" title="Please enter your password" placeholder="******"  name="confirmpassword" id="confirmpassword" class="form-control">
                                <span class="help-block small"><p id="validate-status" style="font-style: italic;"></p></span>
                            </div>

                            <div class="form-group">
                                <label>Account Type</label>
                                  <select name="account_type" tabindex="5" class="form-control">
                                    <option selected="true" disabled="disabled"> Select Option </option>
                                    <option value="2">Researcher / Student</option>
                                    <option value="3">Adviser</option>
                                  </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">What was the name of your first pet?</label>
                                <input type="text" title="Please enter your password" placeholder="Answer the question"  name="question" id="text" class="form-control">
                            </div>
                            <!-- <div class="checkbox">
                                <input type="checkbox" class="i-checks" checked>
                                     Remember login
                                <p class="help-block small">(if this is a private computer)</p>
                            </div> -->
                            <!-- <input type="button" class="btn btn-success btn-block setAlg" value="Next step" disabled="true" id="setAlg" name=""> -->
                            <button  name="submit" value="Submit" class="btn btn-success btn-block">Next step</button>
                            <button  name="submit" value="Cancel" class="btn btn-default btn-block">Already have an account?</button>
                            <!-- <a href="<?= base_url(). 'login/'  ?>" class="btn btn-default btn-block">Already have an account?</a> -->
                        </form>
                </div>
            </div>
        </div>
    </div>
                            <!-- Trappings for Password matching -->
                            <style type="text/css">
                                .a{
                                    color: green;
                                }
                                .b{
                                    color: red;
                                }
                            </style>

                            <script type="text/javascript">
                                // $(document).ready(function() {
                                //   $("#confirmpassword").keyup(validate);
                                //   $("#password").keyup(validateerror);
                                // });


                                // function validate() {
                                //   var password1 = $("#password").val();
                                //   var password2 = $("#confirmpassword").val();
                                //     if (password2 != "") {
                                //        if(password1 != password2) {
                                //         $("button").attr('disabled', true); 
                                //         $("#validate-status").text("Password does not match");
                                //         $("#validate-status").parent().removeClass('a').addClass('b');
                                //        }
                                //        else {
                                //         $("button").attr('disabled', false); 
                                //         $("#validate-status").text("Password match!");
                                //         $("#validate-status").parent().removeClass('b').addClass('a');
                                //        }
                                //     }
                                //     else{
                                //         $("#validate-status").text("");
                                //     }     
                                // }
                                // // End Trappings for Password matching

                                // function validateerror() {
                                //   var password1 = $("#password").val();

                                //     var upperCase= new RegExp('[A-Z]');
                                //     var lowerCase= new RegExp('[a-z]');
                                //     var numbers = new RegExp('[0-9]');


                                //     if(/^[a-zA-Z0-9- ]*$/.test(password1) == false) {
                                //         $("#validateerror3").text('illegal characters.');
                                //     }
                                //     else{
                                //         $("#validateerror3").text("");
                                //     }
                                //     if(password1 != upperCase)  
                                //     {
                                //         $("button").attr('disabled', true); 
                                //         $("#validateerror").text("*Must contain atleast 1 UpperCase letter");
                                //     }
                                //     if ($(this).val().match(upperCase))
                                //     {
                                //         $("#validateerror").text("");
                                //     }
                                //     if(password1 != lowerCase)
                                //     {
                                //         $("button").attr('disabled', true); 
                                //         $("#validateerror1").text("*Must contain atleast 1 LowerCase letter");
                                //     }
                                //     if ($(this).val().match(lowerCase))
                                //     {
                                //         $("#validateerror1").text("");
                                //     }
                                    
                                //     if(password1 != numbers){
                                //         $("button").attr('disabled', true); 
                                //         $("#validateerror2").text("*Must contain atleast 1 Number");
                                //     }
                                //     if ($(this).val().match(numbers))
                                //     {
                                //         $("#validateerror2").text("");
                                //     }
                                //     // if(password1 < 8)
                                //     // {
                                //     //     $("#validateerror3").text("*Password length must be 8 and above");
                                //     // }
                                //     // else if(password1 >= 8)
                                //     // {
                                //     //     $("#validateerror3").text("");
                                //     // }
                                //     if (password1 == "") {
                                //         $("button").attr('disabled', true); 
                                //          $("#validateerror").text("");
                                //         $("#validateerror1").text("");
                                //         $("#validateerror2").text("");
                                //     }
                                //     else if ($(this).val().match(upperCase) && $(this).val().match(lowerCase) &&   $(this).val().match(numbers))
                                //     {
                                //         // $("button").attr('disabled', false); 
                                //         $("#validateerror").text("");
                                //         $("#validateerror1").text("");
                                //         $("#validateerror2").text("");
                                //     }
                                // }

                                
                            </script>
                            