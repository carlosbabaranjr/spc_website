<div id="myCarousel" class="carousel slide" data-ride="carousel">
<?php if ($num_rows_carousel>=1) { ?>
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
      $i = 0;
      foreach ($query_carousel->result() as $row) {
    ?>

    <?php if ($i==0) {
      echo '<li data-target="#myCarousel" data-slide-to="'.$i.'" class="active"></li>';
    } else {
      echo '<li data-target="#myCarousel" data-slide-to="'.$i.'"></li>';
    }
    $i++;
    } 
    ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php
      $i = 0;
      foreach ($query_carousel->result() as $row) {
    ?>

    <?php if ($i==0) { ?>
      <div class="item active">
        <img src="<?= base_url().'img/carousel/'.$row->pic ?>" alt="<?= $row->pic ?>" width="460" height="345">
      </div>
    <?php } else { ?>
      <div class="item">
        <img src="<?= base_url().'img/carousel/'.$row->pic ?>" alt="<?= $row->pic ?>" width="460" height="345">
      </div>
    <?php } 
    $i++;
    } ?>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<?php } else { ?>
<h1>No Images Available</h1>
<?php } ?>
</div>

<div class="col-md-12" style="width: 100%;height: 120px; border: 1px solid #e4948d;margin-top: 20px;border-radius: 5px;background-color: #faf7eb;margin-bottom: 10px;">
  <div class="col-md-4" style="margin-top: -12px;">
    <h3>About Us</h3>
    <p style="margin-top: -10px;text-align: justify;">
      The story of the Sisters of St. Paul de Chartres is a long story of Love extending over 300 years written in a mysterious way and hidden in the hearts of thousands of women. <a href="<?= base_url() ?>webpages/aboutus">[Read More]</a>
    </p>
  </div>
  <div class="col-md-8">
    <div style="margin-top: 20px;margin-left: 50px;">
      <a href="#">
        <img src="<?= base_url() ?>img/link1.png">
      </a>

      <a href="#">
        <img src="<?= base_url() ?>img/link2.png">
      </a>
    </div>
  </div>
</div>

<div class="col-md-12">
	<div class="col-md-8">
		<h1>News and Events</h1>
		<?php foreach ($query_webpages->result() as $row) { 
		$str = strip_tags($row->page_content);
		$page_preview = substr($str, 0, 250).'... '; 
		$page_url = base_url().'webpages/news/'.$row->page_url;
    if ($row->id != 5) {
		?>
  		<img src="<?= base_url().'img/cms/'.$row->pic ?>" class="img-responsive img-thumbnail" style="width:20%; float: left; margin-right: 15px;">

  		<h4><a href="<?= $page_url ?>"><?= $row->page_title ?></a></h4>
  		<p style="text-align: justify;"><?= $page_preview ?><a href="<?= $page_url ?>"><br>[Read More]</a></p>
		<?php }} ?>
	</div>
	<div class="col-md-4" style="background-color: #f1ecd9;border:1px solid #7e7e7e; border-radius: 5px;">
		<h1><i>Welcome!</i></h1>
		<p style="text-align: justify;">Welcome to the Sisters of St. Paul of Chartres East Timor Region website. We are  Sisters of St. Paul, a missionary Congregation, founded in 1696, by Fr. Louis Chauvet, parish priest of  Levesville-la-Chenard, a little village in the region of Beauce, some 80 kilometers southeast of Paris.</p>
    <p style="text-align: justify;">Our first house in the center of the village belonged to him.</p>
    <p style="text-align: justify;">Our first chapel was the parish church, our first field of apostolate, the surrounding hamlets.</p>
    <p style="text-align: justify;">Our first mission consisted in working to improve the human and spiritual level of the villagers by educating the girls and visiting the poor and the sick. </p>
    <p style="text-align: justify;"><b>Now, we are present in 40 countries spread out in the 5 continents of the world</b>, still faithful to our first mission but intensely to make the presence of Jesus felt in the many and different faces of  the last, the least and the lost.</p>
    <p style="text-align: justify;"><i>The love of Christ continues to impel us to manifest his tenderness and goodness to all. </i></p>

	</div>
</div>

