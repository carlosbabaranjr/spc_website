<div class='box box-info'>
    <div class='box-body pad'>
		<h1 class="page-header"><?= $headline ?></h1>
		<p>
			Are you sure you want to delete the account?
		</p>
		
		<a href="<?= base_url(). 'test/delete/'.$update_id; ?>">
			<button class="btn btn-success">Yes - Delete Account</button>
		</a>
		&nbsp; &nbsp;
		<a href="<?= base_url(). 'test/create/'.$update_id ?>">
			<button class="btn btn-default">Cancel</button>
		</a>
	</div>
</div>