<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mdl_homepage extends CI_Model
{

function __construct()
{
    parent::__construct();
}

function get_table1()
{
    $table = "carousel";
    return $table;
}

function get_table2()
{
    $table = "webpages";
    return $table;
}

function get_carousel($order_by)
{
    $table = $this->get_table1();
    $this->db->order_by($order_by);
    $query=$this->db->get($table);
    return $query;
}

function get_webpages($order_by)
{
    $table = $this->get_table2();
    $this->db->order_by($order_by);
    $query=$this->db->get($table);
    return $query;
}

}// end class