-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 05, 2017 at 04:44 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spc_website`
--

-- --------------------------------------------------------

--
-- Table structure for table `carousel`
--

CREATE TABLE `carousel` (
  `id` int(11) NOT NULL,
  `pic` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carousel`
--

INSERT INTO `carousel` (`id`, `pic`) VALUES
(19, 'sisters.jpg'),
(20, 'sisters2.JPG'),
(21, 'sister3.JPG'),
(22, 'sisters4.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `firstname` varchar(65) NOT NULL,
  `lastname` varchar(65) NOT NULL,
  `email` varchar(65) NOT NULL,
  `company` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `firstname`, `lastname`, `email`, `company`) VALUES
(1, 'Carlos', 'Babaran Jr.', 'cbabaranjr@gmail.com', 'spup');

-- --------------------------------------------------------

--
-- Table structure for table `webpages`
--

CREATE TABLE `webpages` (
  `id` int(11) NOT NULL,
  `page_url` varchar(255) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_keywords` text NOT NULL,
  `page_description` text NOT NULL,
  `page_content` text NOT NULL,
  `pic` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `webpages`
--

INSERT INTO `webpages` (`id`, `page_url`, `page_title`, `page_keywords`, `page_description`, `page_content`, `pic`) VALUES
(3, 'HOLY-FATHER-POPE-FRANCIS', 'HOLY FATHER, POPE FRANCIS', 'HOLY FATHER, POPE FRANCIS', 'Greetings of Joy and Thanksgiving to our beloved Holy Father Pope Francis.', '<p>Pope Francis said: The embrace of God’s love silences our sins, no matter how many and not all love comes from God but He is the true love.</p>\r\n\r\n<p>He went on to stress how “the Lord is waiting for me, the Lord wants me to open the door of my heart” and we must have this certainty that He will wait for us just as we are and not as we are told to be.</p>\r\n', 'blessingpope2.jpg'),
(4, 'What-is-Charism', 'What is Charism', 'What is Charism', 'A short explanation on charism', '<p>Charism or spiritual gift is simply the Greek word used in the New Testament for "favor" or "gratuitous gift". Charisms, or spiritual gifts, are special abilities given to Christians by the Holy Spirit to enable them to be powerful channels of God&#39;s love and redeeming presence in the world. Whether extraordinary or ordinary, charisms are to be used in charity or service to build up the Church (Catechism of the Catholic Church, 2003). Charisms are not "inborn" or inherited from our parents, but are given to us by the Holy Spirit, whom we received through baptism and confirmation, two of the three sacraments of initiation. Charisms are also supernaturally empowered.</p>\r\n\r\n<p>They are focused outward and enable Christians to bear results for the Kingdom of God above and beyond our normal human abilities. As disciples, we offer our entire selves, including our personalities, natural talents, education, life experience, and background to God to be used for his purposes. Our natural talents can become wonderful tools for God&#39;s purposes, and sometimes an already existing natural gift is transformed by the Holy Spirit into a supernaturally empowered charism. But when we serve God, we are not limited to just the gifts with which we were born! Some charisms may seem &#39;extraordinary&#39; (such as prophecy, healing, or discernment of spirits) and others quite &#39;ordinary&#39; (such as administration, service, hospitality, or mercy), but all charisms are supernaturally empowered.</p>\r\n\r\n<p>We use our charisms together with our natural talents and all that we are to serve God and our neighbor. 1 CCC 2003</p>\r\n', 'mm.JPG'),
(5, 'About-Us', 'About Us', 'SPC About Us', 'About Us Page Sisters of St. Paul de Chartres East Timor Region', '<h1><strong>The story of the Sisters of St. Paul de Chartres is a long story of Love extending over 300 years written in a mysterious way and hidden in the hearts of thousands of women.</strong></h1>\n\n<p>In 1696, Father Louis Chauvet, a young, humble parish priest of Levesville-la-Chenard, a small, poverty-stricken remote village in France, invited three young girls, together with Marie Anne de Tilly, a gentlewoman from Allaines, to form a community to care for the sick and to comfort the lonely and the poor. Soon, a little school was started and the children began to learn and to develop a sense of their own dignity, despite the desperate poverty of the times. Levesville is not far from the great Cathedral of Chartres, and the Bishop of Chartres, Monsignor Paul Godet des Marais, took a keen interest in the little community. In 1710, after the death of Fr. Chauvet, the Bishop took the Sisters under his protection and gave them his name. The Sisters were then officially known as “ Sisters of St. Paul de Chartres”.</p>\n\n<p>From the earliest days of their fledgling community, the Sisters drew inspiration from the writings and spirit of St. Paul. They resolved to follow his footsteps, eager to spread the Good News of Jesus. The Congregation has no territorial preference and responds with fidelity to the call of the Church, to leave everything in order to follow Christ and to serve their brothers and sisters under every horizon, with a special love for the most neglected. Their major services include education, nursing, and all kinds of pastoral work. In practical terms, this has often meant working in leprosarium, prisons, visiting and serving in remote, poverty-stricken areas.</p>\n\n<p>In 1727, in response to the personal request of King Louis XV, the first four missionaries set out for French Guyana, in South America, to the notorious Devil’s Island. After the French Revolution, despite the suppression of the Church, Napoleon called on the Sisters of St. Paul de Chartres to go back to French Guyana where the need was great.</p>\n\n<p>Now, in the 21st Century, the Sisters of St. Paul de Chartres serve in 34 countries all over the world, ready to answer both spiritual and human needs, anywhere and at any time. In Vietnam, where one of the largest groups of Sisters can be found, over 1,000 Sisters work in hospitals, schools, orphanages and aged care facilities. Sisters from Hong Kong were sent in 1984 to establish communities in Australia. At present, the Sisters’ work in Australia includes an international college, a retirement village, a student hostel and local parish ministry.</p>\n\n<p>True to its humble origins, the Congregation of the Sisters of St. Paul de Chartres aspires to live the poverty and simplicity of Christ’s Gospel in genuine charity and humility, daring to trust in God alone.</p>\n\n<p>As Sisters of St. Paul de Chartres, consecrated women with the daring spirit of St. Paul, we believe the Spirit of God has called us to this mission and ministry :</p>\n\n<ul>\n <li>To participate in Christ’s Paschal Mystery</li>\n <li>To follow a Christ-centred spirituality, nourished by the Eucharist, the Word of God, and personal and communal prayer</li>\n <li>To live a communal, fraternal life, sharing our faith and supporting one another with loving concern</li>\n <li>To be a joyful and welcoming presence among people, giving hospitality and inviting them to a deeper love of God</li>\n <li>To live and work with the spirit of St. Paul, “ all to all ” among the people of diverse cultures, witnessing to Christ’s love through our words and actions</li>\n <li>To respond with tranquil daring and openness to the contemporary needs of evangelisation through our present apostolate of education, pastoral care, health and aged care</li>\n <li>To respond as best as we can to the needs of the new millennium and to develop new ministries to meet these needs</li>\n</ul>\n', 'bv01226.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carousel`
--
ALTER TABLE `carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webpages`
--
ALTER TABLE `webpages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carousel`
--
ALTER TABLE `carousel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `webpages`
--
ALTER TABLE `webpages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
